<!DOCTYPE html>
<html>
    <head>
        <title>Form</title>
        </head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

<!--membuat form-->
<form action="/post" method="POST">
    @csrf
    <label for="fname">First Name</label> 
    <br><br>
    <input type="text" name="awal" id="fname">
    <br><br>
    <label for="lname">Last Name</label>
    <br><br>
    <input type="text" name="akhir" id="lname">
    <br><br>
    <label>Gender: </label><br><br>
    <input type="radio" id="Male" name="gender"><label for="Male">Male</label>
    <br>
    <input type="radio" id="Female" name="gender"><label for="Female">Female</label>
    <br>
    <input type="radio" id="Other" name="gender"><label for="Other">Other</label>
    <br><br>
    <label>Nasionality</label>
    <br><br>
    <select>
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="singapura">Singapura</option>
        <option value="laos">Laos</option>
        <option value="vietnam">Vietnam</option>
        <option value="thailand">Thailand</option>
        <option value="timorleste">TimorLeste</option>
        <option value="kamboja">Kamboja</option>
        <option value="koreaselatan">Korea Selatan</option>
        <option value="jepang">Jepang</option>
    </select>
    <br><br>
    <label>Language Spoken</label>
    <br><br>
    <input type="checkbox" id="bahasa indonesia"><label for="bahasa indonesia">Bahasa Indonesia</label><br>
    <input type="checkbox" id="english"><label for="english">English</label><br>
    <input type="checkbox" id="other"><label for="other">Other</label>
    <br><br>
    <label>Bio:</label><br>
    <textarea cols="100" rows="7"></textarea>

    <br><br>
    <button type="submit">Submit</button>
</form>
</body>
</html>