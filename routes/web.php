<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/',function(){
    return view('template-laravel.data.tabel');
});
Route::get('/data-table',function(){
    return view('template-laravel.data.data-table');
});

Route::get('/HOME',function (){
   return view('home');
});

Route::get('/home','HomeController@home');
Route::get('/register','AuthController@register');
Route::post('/post','AuthController@post');